uniform float BarrelPower;
uniform float PI = 3.14159265;
uniform float Alpha;
sampler s0;

// Given a vec2 in [-1,+1], generate a texture coord in [0,+1]
float2 Distort(float2 p)
{
    float theta  = atan(p.y/p.x);
	if (p.x < 0)
		theta += PI;
    float radius = length(p);
    radius = pow(radius, BarrelPower);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
	//return p;
    return 0.5 * (p + 1.0);
}

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
	// Rescale texture coordinates from [0,+1] to [-1,+1]
	coords = coords*2 - 1.0;

	float2 distorted_coords = Distort(coords);
	float4 color = tex2D(s0, distorted_coords);
	if (color.a > 0)
		color.a = Alpha;
	return color;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}