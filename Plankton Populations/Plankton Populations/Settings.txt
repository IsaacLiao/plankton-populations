[Screen]
fullscreen = true					# whether to use fullscreen mode (true/false)
show_mouse = true					# whether to show the mouse cursor (true/false)
resolution_x = 1920					# preferred resolution width (pixels)
resolution_y = 1080					# preferred resolution height (pixels)
antialiasing = true					# smooths jagged pixel edges (true/false)
	
[Input]	
mouse_input = true					# whether to use mouseclicks as input (true/false)
									#	NOTE: input options below here do nothing if mouse_input is true
use_orientation = false				# whether to use TUIO orientation information
use_fiducials = true				# whether to use fiducial ID's instead of session ID's	
touch_time = 50						# how long a button needs to be touched to actually activate (ms)
use_global_calibration = true		# whether to use the following values to offset all markers
global_x_shift = -4					# (pixels)
global_y_shift = 0
swipes_only = false					# open and close buttons only respond to swipes

use_individual_calibration = false	# whether to use the following values to offset data locations from individual markers

#	NOTE: does nothing when use_fiducials is false
#	NOTE: will ignore symbol ID's not listed here
# (rotation, distance, rotation; degrees,pixels,degrees)
# First rotation and distance move the marker to the data point.
# Second rotation is around the data point to match the physical object.
marker_0 = 	90,30,225
marker_1 = 	90,30,225
marker_2 = 	225,40,0
marker_3 = 	0,0,0					
marker_4 = 	45,40,180
marker_6 = 	45,40,180
marker_5 = 	0,40,0
marker_7 = 	90,30,225
marker_8 = 	90,30,225
marker_9 = 	90,30,225

[Circles]
image_border = false						# use an image for the border of circles
lens_shader = false							# use a shader to simulate a lens effect
lens_power = 1.3							# exponential magnification power of lens (float); does nothing if lens_shader is false
radius = 95									# radius of magnifying glass circles (pixels)
radius_overscan = 95						# extra radius outside of visible circle that plankton are still allowed to be
											# 		positioned in (pixels)
velocity = 200								# how fast circles move (pixels / frame)
border_width = 3							# width of magnifying glass borders (pixels)
border_color = 127,127,127,255				# color and opacity of magnifying glass borders (0-255; red, green, blue, alpha)
background_color = 0,25,50,210				# color and opacity of circle background (0-255; red, green, blue, alpha)
on_land_background_color = 0,25,50,128		# color and opacity of circle background over land (0-255; red, green, blue, alpha)
open_time = 0         						# how long circles stay open with no input (milliseconds)
fadein_time = 1000							# how long it takes circles to fade in (milliseconds)
fadeout_time = 1000							# how long it takes circles to fade out (milliseconds) 5000
max_number = 3								# max number of circles that can be open at once
detection_radius = 250						# if touched within this radius, move circle instead of opening a new one;
											# 		doesn't do anything if draw_offset is true (pixels)		
position_change_threshold = 0				# number of pixels that an object has to move in a single update to actually change the circle position (pixels, can be fractional)

[Guide]
show_callout = true					# whether to show the guide (true/false)
vertical_adjust = 4					# vertical offset of guide , positive = down (pixels)
horizontal_adjust = -24				# horizontal offset of guide when closed, mostly the width of the "guide" tab (pixels)
horizontal_hide = 0			  	    # how far to the right of the circle center to start drawing the callout (pixels)
detection_radius = 30				# detection radius for guide button (pixels)
opening_time = 500					# how long it takes the guide to open (milliseconds)
closing_time = 500					# how long it takes the guide to close (milliseconds)
close_coords = 388,-50				# location of guide button when callout is open, relative to circle center
open_coords = 100,-50				# location of guide button when callout is closed, relative to circle center
tab1_coords = 107,-90				# location of first tab when callout is open, relative to circle center
tab2_coords = 168,-90				# location of second tab when callout is open, relative to circle center
tab3_coords = 229,-90				# location of third tab when callout is open, relative to circle center
tab4_coords = 290,-90				# location of fourth tab when callout is open, relative to circle center
close_coords_reflected = -393,-50	# location of buttons and tabs when callout is reflected to the left side of circle
open_coords_reflected = -100,-50	# ||
tab1_coords_reflected = -114,-90	# ||
tab2_coords_reflected = -175,-90	# ||
tab3_coords_reflected = -236,-90	# ||
tab4_coords_reflected = -297,-90	# ||
tab_height = 30						# height of tabs, for touch detection purposes (pixels)
tab_width = 55						# width of tabs, for touch detection purposes (pixels)

[Offset]
offset_distance = 25				# distance between edge of zoomed circle and edge of data location circle (pixels)
radius = 1							# radius of small circle showing data location
border_width = 3					# width of small circle border (pixels)
border_color = 158,159,160,255		# color of small circle border (8-bit rgba)
detection_radius = 40				# if touched within this radius, move circle instead of opening a new one;
									#		doesn't do anything if draw_offset is false (pixels)
tangent_lines_width = 3				# width of tangent lines (pixels)

[Timeline]
position = 1200,20							# screen coordinates to draw the timeline slider at (pixels, relative to
											#		bottom left corner)200,30
month_hash_height = 10						# height of month hash marks in pixels
month_name_offset = 10						# how far above timeline to write month names (pixels)
scrubber_height = 20						# height of scrubber line in pixels
scrubber_width = 3							# width of scrubber line in pixels
one_year_only = true						# whether to show all 6 years or just 1 year (true/false)
circular = false							# whether to show circular timeline, only 1 year (true/false)
circular_position = 1700,65					# screen coordinates where timeline should be drawn
											#		(center of circle if circular) (pixels, relative to bottom left corner)
circular_radius = 40
linear = false								# whether to display timeline as a line (older version)
month_name = true							# whether to display month name in bottom center
month_name_center_width = 200
month_name_center_transition_width = 100
month_name_blank_edge_width = 200
month_name_edge_transition_width = 100
month_name_spacing = 200
month_name_static = true					# whether to display month names without moving them
month_name_static_spacing = 150				
month_name_static_marker_offset = 20		# how far below the month names to draw the time marker
month_name_expand_from_baseline = true		# otherwise centered
month_name_y = 45							# y-position to draw month name, relative to bottom of screen (pixels)
month_name_current_color = 255,255,255		# color of current month name text
month_name_other_color = 64,64,64			# color of other month name text
mirror = false								# whether to draw all timeline elements on the other side upside down as well

[Plankton]
max_total = 9000										# maximum number of plankton across all rings
max_per_circle = 3000									# maximum number of plankton PER CIRCLE
phosphorus_conversions = 3.1e7, 3.875e6, 3.875e3, 248	# conversion factors from mmol p/m^3 to cell number
														#	(assumes cell diameters of 1, 2, 20, 50 um)
count_conversions = 1, 1, 2.5, 5 						# amount to multiply final number of phytoplankton of each type by
opacity = 200 											# opacity of plankton, 0-255
fadein_time = 1000										# how long it takes plankton to fade in, in milliseconds
fadeout_time = 1000										# how long it takes plankton to fade out, in milliseconds
#sizes = 1, 2, 20, 50									# actual proportional diameters
#sizes = 5,10,100,250									# actual proportional diameters * 5
#sizes = 10,20,200,500									# actual proportional diameters * 10
sizes = 3, 5, 35, 50									# how large to render plankton (diameters in pixels)
														# 	actual proportional diameters: *10 for first 2,
														#	*4 for third type, *2 for fourth type
[Dashboard]
show_light = false				# NOTE: final version doesn't have data files for light, temp, silica, nitrate
show_temperature = false
show_silica = false														
show_nitrate = false
orientation = 270				# angular position of first readout with respect to zoomed circle; 0 = 9 o'clock (degrees)
spacing = 25					# angular spacing between readouts (degrees)
size = 30						# vertical size of readouts (pixels) 40
distance = 30					# distance from circle border to readout center (pixels)
label_distance = 20				# how far below readouts to write labels (pixels)
icon_color = 255,255,255,255	# color and opacity of icons (8-bit rgba)
label_color = 255,255,255,255	# color and opacity of label text (8-bit rgba)
														
[Tools]
num_temptools = 0		
num_nutrienttools = 0	

[Movie]
pause_when_circles_shown = false	# (true/false)
blue_water = false					# use older blue water movie (true/false)
									# 		NOTE: final version doesn't have blue water movie files
blue_water_saturated = false		# use even older blue water movie with saturated plankton colors (true/false)
									#		NOTE: does nothing if blue_water is false
slower_movie = true					# use slower movie, 5 days/second instead of 15 days/second (true/false)
									#		NOTE: does nothing if blue_water is true
									#		NOTE: final version doesn't have faster movie file

[Debug]
show_running_slowly_indicator = false	# draw an hourglass icon on frames when the system can't keep up (true/false)
show_touches = false					# draw touches on screen
show_hitboxes = false					# show button touch areas

[Crosshairs]
crosshairs_mode = true			# whether to draw circles centered on touch point with crosshairs; settings in this group do nothing if false (true/false)
color = 172,172,172,255			# crosshairs color (0-255, RGBA)
width = 1						# width of crosshairs (pixels)
length = 30						# length of crosshairs (pixels)
medium_threshold_velocity = 10	# medium threshold velocity; less is considered SLOW velocity (pixels/second)
medium_opacity = 90				# opacity of plankton layer when MEDIUM (0-255)
on_medium_fade_time = 250		# when transitioning to MEDIUM, how long it takes to fade the plankton layer to medium_opacity (milliseconds)
slow_delay_time = 750			# how long velocity needs to stay under medium_velocity in order to transition to SLOW state (milliseconds)
on_slow_fade_time = 1000		# when transitioning to SLOW, how long it takes to fade the plankton layer back in (milliseconds)
ring_up_delay_time = 250		# how long ring needs to be up before RING_UP transition happens (milliseconds)
on_ring_up_zoom_time = 300		# how fast the RING_UP zoom out animation happens (milliseconds)
on_ring_down_zoom_time = 750	# how fast the RING_DOWN zoom in animation happens (milliseconds)
anti_jitter_delay = 3000		# if ring is in SLOW for this amount of time, stop updating the apparent position until it moves fast enough to transition out of SLOW (milliseconds)

[Continents]
color = 16,16,16,255			# continents color (0-255, RGBA)

[TouchOnly]
touch_only = true				# mode that uses touches to move circles instead of objects (true/false)
medium_threshold_velocity = 1	# medium threshold velocity; less is considered SLOW velocity (pixels/second)
